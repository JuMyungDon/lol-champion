import Vue from 'vue'

import LoadingIcon from '~/components/common/loading-icon'
Vue.component('LoadingIcon', LoadingIcon)

import LolChampInfo from '~/components/lol-champ-info'
Vue.component('LolChampionInfo', LolChampInfo)
